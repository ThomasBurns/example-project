/*
 * UartBuffer.c
 *
 * Created: 24-Aug-14 16:05:57
 *  Author: thomas
 */
#include "asf.h"
#include <stdint.h>
#include "UartBuffer.h"
//#include "sam3s4a.h"

char TxBuffer[1024];
char RxBuffer[1024];
volatile uint16_t TxIn, TxOut;
volatile uint16_t RxIn, RxOut;
const uint16_t TxSize = 1024;
const uint16_t RxSize = 1024;

/* ============================================= */
const sam_usart_opt_t usart_console_settings = {
  115200,
  US_MR_CHRL_8_BIT,
  US_MR_PAR_NO,
  US_MR_NBSTOP_1_BIT,
  US_MR_CHMODE_NORMAL,
  /* This field is only used in IrDA mode. */
  0
};

#define ALL_INTERRUPT_MASK  0xffffffff
/* ============================================= */
void UartBuffer_Init(void)
{
  sysclk_enable_peripheral_clock(ID_USART1);
  usart_init_rs232(USART1, &usart_console_settings, sysclk_get_cpu_hz());
  usart_disable_interrupt(USART1, ALL_INTERRUPT_MASK);
  usart_enable_tx(USART1);
  usart_enable_rx(USART1);

  usart_enable_interrupt(USART1, US_IER_RXRDY);
  NVIC_EnableIRQ(USART1_IRQn);

  TxIn = TxOut = 0;
  RxIn = RxOut = 0;
}

/* ============================================= */
void UartBuffer_Interrupt(void)
{
  static uint32_t ul_status;
  uint32_t c;

  /* Read USART Status. */
  ul_status = usart_get_status(USART1);

  if ( ul_status & US_CSR_TXBUFE ) {
    if ( TxOut != TxIn ) {
      usart_putchar(USART1, TxBuffer[TxOut]);
      TxOut++;
      if ( TxOut >= TxSize ) {
        TxOut = 0;
      }
    } else {
      usart_disable_interrupt(USART1, US_IER_TXRDY);
    }
  }
  if ( usart_is_rx_ready(USART1)) {
    usart_getchar(USART1, &c);
    RxBuffer[RxIn] = c;
    RxIn++;
    if ( RxIn >= RxSize ) {
      RxIn = 0;
    }
  }
}

/* ============================================= */
char UartBuffer_TxFull(void)
{
  return ((TxIn+1)== TxOut )? 1: 0;
}

void UartBuffer_PutChar(char c)
{
  cpu_irq_disable();
  uint16_t out_next = (TxOut+1);
  if ( out_next != TxIn ) {
    TxBuffer[TxIn] = c;
    TxIn++;
    if ( TxIn >= TxSize ) {
      TxIn = 0;
    }
    usart_enable_interrupt(USART1, US_IER_TXRDY);
  }
  UartBuffer_Interrupt();
  cpu_irq_enable();
}

void UartBuffer_PutString(const char *s)
{
  while ( *s ) {
    UartBuffer_PutChar(*s++);
  }

}

void UartBuffer_PutBuffer(const char *buf, int len)
{
  int i;

  for ( i = 0; i < len; i ++) {
    UartBuffer_PutChar(buf[i]);
  }
}

/* ============================================= */
char UartBuffer_RxReady(void)
{
  return ( RxIn != RxOut )? 1: 0;
}

char UartBuffer_GetChar(void)
{
  char c = 0;

  cpu_irq_disable();
  if ( UartBuffer_RxReady() ) {
    c = RxBuffer[RxOut];
    RxOut++;
    if ( RxOut >= RxSize ) {
      RxOut = 0;
    }
  }
  cpu_irq_enable();
  return c;
}
