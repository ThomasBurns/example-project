/**
 * \file
 *
 * \brief User board initialization template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include "asf.h"
#include <board.h>
#include <conf_board.h>

#define WDT_PERIOD                        3000

void board_init(void)
{
  /* This function is meant to contain board-specific initialization code
   * for, e.g., the I/O pins. The initialization can rely on application-
   * specific board configuration, found in conf_board.h.
   */

  // LEDs
  PIOA->PIO_PER = PIO_PA17;	// Enable Register
  PIOA->PIO_OER = PIO_PA17;
  PIOA->PIO_CODR = PIO_PA17;

  PIOA->PIO_PER = PIO_PA18;	// Enable Register
  PIOA->PIO_OER = PIO_PA18;
  PIOA->PIO_CODR = PIO_PA18;

  // Watchdog
  uint32_t timeout_value = wdt_get_timeout_value(WDT_PERIOD * 1000, BOARD_FREQ_SLCK_XTAL);
  /* Configure WDT to trigger an interrupt (or reset). */
  uint32_t wdt_mode = WDT_MR_WDRPROC |	/* WDT fault resets processor only. */
                      WDT_MR_WDRSTEN  |
                      WDT_MR_WDDBGHLT |	/* WDT stops in debug state. */
                      WDT_MR_WDIDLEHLT;	/* WDT stops in idle state. */

  // Initialize WDT with the given parameters.
  wdt_init(WDT, wdt_mode, timeout_value, timeout_value);

  // Set USART pins to use peripheral driver
  pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21A_RXD1 | PIO_PA22A_TXD1 );
}
