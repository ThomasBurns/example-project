/*
 * UartBuffer.h
 *
 * Created: 24-Aug-14 16:06:54
 *  Author: thomas
 */

#ifndef __UART_BUFFER__
#define __UART_BUFFER__

void UartBuffer_Init(void);

void UartBuffer_Interrupt(void);

char UartBuffer_TxFull(void);
void UartBuffer_PutChar(char c);
void UartBuffer_PutString(const char *s);
void UartBuffer_PutBuffer(const char *buf, int len);


char UartBuffer_RxReady(void);
char UartBuffer_GetChar(void);

#endif
