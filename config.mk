#
# Copyright (c) 2011 Atmel Corporation. All rights reserved.
#
# \asf_license_start
#
# \page License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. The name of Atmel may not be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# 4. This software may only be redistributed and used in connection with an
#    Atmel microcontroller product.
#
# THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
# EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# \asf_license_stop
#

# Path to top level ASF directory relative to this project directory.
PRJ_PATH = ./
PROJECT_TYPE = flash

# Target CPU architecture: cortex-m3, cortex-m4
ARCH = cortex-m3

# Target part: none, sam3n4 or sam4l4aa
PART = sam3s4b

# Application target name. Given with suffix .a for library and .elf for a
# standalone application.
TARGET_FLASH = TestProject.elf
#TARGET_SRAM = TestProject_sram.elf

# List of C source files.
CSRCS = \
	./ASF/common/services/clock/sam3s/sysclk.c \
	./ASF/sam/drivers/pmc/pmc.c \
	./ASF/sam/drivers/pmc/sleep.c \
	./ASF/common/utils/interrupt/interrupt_sam_nvic.c \
	./ASF/sam/drivers/pio/pio.c \
	./ASF/sam/drivers/pio/pio_handler.c \
	./ASF/sam/drivers/tc/tc.c \
	./ASF/sam/drivers/usart/usart.c \
	./ASF/sam/drivers/wdt/wdt.c \
	./ASF/sam/utils/cmsis/sam3s/source/templates/exceptions.c \
	./ASF/sam/utils/cmsis/sam3s/source/templates/gcc/startup_sam3s.c \
	./ASF/sam/utils/cmsis/sam3s/source/templates/system_sam3s.c \
	./ASF/sam/utils/syscalls/gcc/syscalls.c \
	./UartBuffer.c \
	./init.c \
	./main.c

# List of assembler source files.
ASSRCS =

# List of include paths.
INC_PATH = \
	./include/                                       \
	./config/                                        \
	./ASF                                            \
	./ASF/common                                     \
	./ASF/common/boards                              \
	./ASF/common/services/                           \
	./ASF/common/services/clock                      \
	./ASF/common/services/clock/sam3s                \
	./ASF/common/utils/                              \
	./ASF/common/utils/interrupt                     \
	./ASF/sam                                        \
	./ASF/sam/drivers                                \
	./ASF/sam/drivers/pio                            \
	./ASF/sam/drivers/pmc                            \
	./ASF/sam/drivers/tc                             \
	./ASF/sam/drivers/usart                          \
	./ASF/sam/drivers/wdt                            \
	./ASF/sam/utils                                  \
	./ASF/sam/utils/cmsis                            \
	./ASF/sam/utils/cmsis/sam3s/include              \
	./ASF/sam/utils/cmsis/sam3s/include/component    \
	./ASF/sam/utils/cmsis/sam3s/include/instance     \
	./ASF/sam/utils/cmsis/sam3s/include/pio          \
	./ASF/sam/utils/cmsis/sam3s/source               \
	./ASF/sam/utils/cmsis/sam3s/source/templates     \
	./ASF/sam/utils/cmsis/sam3s/source/templates/gcc \
	./ASF/sam/utils/header_files                     \
	./ASF/sam/utils/preprocessor                     \
	./ASF/sam/utils/syscalls                         \
	./ASF/sam/utils/syscalls/gcc                     \
	./ASF/thirdparty                                 \
	./ASF/thirdparty/CMSIS                           \
	./ASF/thirdparty/CMSIS/Include                   \
	./ASF/thirdparty/CMSIS/Lib                       \
	./ASF/thirdparty/CMSIS/Lib/GCC

# Additional search paths for libraries.
LIB_PATH =  \
	./ASF/thirdparty/CMSIS/Lib/GCC \
	./lib

# List of libraries to use during linking.
LIBS =  \
       Svartalfar  \
       arm_cortexM3l_math                                 \
       m

# Path relative to top level directory pointing to a linker script.
LINKER_SCRIPT_FLASH = ./flash.ld
#LINKER_SCRIPT_SRAM  = sam/utils/linker_scripts/sam3s/sam3s4/gcc/sram.ld

# Path relative to top level directory pointing to a linker script.
#DEBUG_SCRIPT_FLASH = sam/boards/sam3s_ek/debug_scripts/gcc/sam3s_ek_flash.gdb
#DEBUG_SCRIPT_SRAM  = sam/boards/sam3s_ek/debug_scripts/gcc/sam3s_ek_sram.gdb

# Project type parameter: all, sram or flash
PROJECT_TYPE        = flash

# Additional options for debugging. By default the common Makefile.in will
# add -g3.
DBGFLAGS =

# Application optimization used during compilation and linking:
# -O0, -O1, -O2, -O3 or -Os
OPTIMIZATION = #-O1

# Extra flags to use when archiving.
ARFLAGS =

# Extra flags to use when assembling.
ASFLAGS =

# Extra flags to use when compiling.
CFLAGS =

# Extra flags to use when preprocessing.
#
# Preprocessor symbol definitions
#   To add a definition use the format "-D name[=definition]".
#   To cancel a definition use the format "-U name".
#
# The most relevant symbols to define for the preprocessor are:
#   BOARD      Target board in use, see boards/board.h for a list.
#   EXT_BOARD  Optional extension board in use, see boards/board.h for a list.
CPPFLAGS = \
       -D ARM_MATH_CM3=true                               \
       -D BOARD=USER_BOARD                                \
       -D __SAM3S4B__                                     \
       -D printf=iprintf                                  \
       -D scanf=iscanf

# Extra flags to use when linking
LDFLAGS = \

# Pre- and post-build commands
PREBUILD_CMD =
POSTBUILD_CMD =
