/**
 * \file
 *
 * \brief Empty user application template
 *
 */

#include "asf.h"

#include <svartalfar/Svartalfar.h>
#include <svartalfar/Timing.h>
#include <svartalfar/Clock.h>
#include <svartalfar/Logging.h>
#include <svartalfar/Syslog.h>

#include "UartBuffer.h"

#define FIRMWAR_VER_MAJOR  0
#define FIRMWAR_VER_MINOR  1

/* ======================================= */
ClockDate_t DefaultDate = {
  .day = 1,
  .month = 1,
  .year = 2015
};

ClockTime_t DefaultTime = {
  .seconds = 0,
  .minutes = 0,
  .hours = 9
};

/* ======================================= */
// Interrupt Handlers
void SysTick_Handler(void)
{
  Timing_Increment();
}

void USART1_Handler(void)
{
  UartBuffer_Interrupt();
}

/* ======================================= */
static void Init_Hardware(void)
{
  SysTick_Config(SystemCoreClock / 1000);
  UartBuffer_Init();
  Timing_LoadValue(0);
  SystemClock_Init(DefaultTime, DefaultDate);

  Log_SetEnable(1);
  UartBuffer_PutString("USART Online\r\n");
  LogPrintf(__FILE__, "Log Online" );
  Syslog(lDiag, __FILE__, "Syslog Online" );

  Syslog(lDiag, __FILE__, "Firmware: %d.%02d", FIRMWAR_VER_MAJOR, FIRMWAR_VER_MINOR );
  Syslog(lDiag, __FILE__, "libSvartalfar: %d.%02d", __SVARTALFAR_VER_MAJOR__, __SVARTALFAR_VER_MINOR__ );
}



int main (void)
{
  uint8_t state = 0;
  uint32_t tmr_1ms = 0;
  uint32_t tmr_1sec = 0;

  sysclk_init();
  board_init();
  Init_Hardware();

  while (1) {
    if ( Timing_CheckDelay(tmr_1ms, 1) ) {
      tmr_1ms = Timing_ReadStamp();
      tmr_1sec ++;
      SysClock_Run(1);
    }

    if ( tmr_1sec >= 1000 ) {
      tmr_1sec -= 1000;
      wdt_restart( WDT);
      if ( state ) {
        Syslog(lDebug, __FILE__, "Tick" );
        PIOA->PIO_SODR = PIO_PA17;
        PIOA->PIO_CODR = PIO_PA18;
        state = 0;
      } else {
        Syslog(lDebug, __FILE__, "Tock" );
        PIOA->PIO_CODR = PIO_PA17;
        PIOA->PIO_SODR = PIO_PA18;
        state = 1;
      }
    }
  }
}

void Log_PrintMsg(const char *msg)
{
  UartBuffer_PutString(msg);
}

void Syslog_Print(LogLevel level, const char *msg)
{
  UartBuffer_PutString(msg);
}
